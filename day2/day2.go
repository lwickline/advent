package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func checkPass(lr string, ur string, letter string, password string) bool {
	occurrences := strings.Count(password, letter)
	ilr, err := strconv.Atoi(lr)
	if err != nil {
		log.Fatal(err)
	}
	iur, err := strconv.Atoi(ur)
	if err != nil {
		log.Fatal(err)
	}
	if occurrences >= ilr && occurrences <= iur {
		return true
	}
	return false
}

func checkPos(lr string, ur string, letter string, password string) bool {
	ilr, err := strconv.Atoi(lr)
	if err != nil {
		log.Fatal(err)
	}
	iur, err := strconv.Atoi(ur)
	if err != nil {
		log.Fatal(err)
	}
	spass := strings.Split(password, "")
	if spass[ilr-1] == letter && spass[iur-1] != letter {
		return true
	} else if spass[ilr-1] != letter && spass[iur-1] == letter {
		return true
	}

	return false
}

func main() {
	var n []string
	data, err := os.Open("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		if err = data.Close(); err != nil {
		}
	}()

	s := bufio.NewScanner(data)
	for s.Scan() {
		data := s.Text()
		if err != nil {
			log.Fatal(err)
		}
		n = append(n, data)
	}
	count := 0
	bad := 0
	pos := 0
	badPos := 0
	for i := 0; i < len(n); i++ {
		sdata := strings.Split(n[i], " ")
		lr := strings.Split(sdata[0], "-")[0]
		ur := strings.Split(sdata[0], "-")[1]
		letter := strings.Split(sdata[1], ":")[0]
		password := sdata[2]
		if checkPass(lr, ur, letter, password) {
			count++
		} else {
			bad++
		}
		if checkPos(lr, ur, letter, password) {
			pos++
		} else {
			badPos++
		}
	}
	fmt.Printf("Good passwords %v\n", count)
	fmt.Printf("Bad passwords %v\n", bad)
	fmt.Printf("Good Positions %v\n", pos)
	fmt.Printf("Bad Positions %v\n", badPos)
}
