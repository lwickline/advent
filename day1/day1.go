package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
)

func sum2(n1 int, n2 int) (int, bool) {
	if n1+n2 == 2020 {
		fmt.Printf("First Number: %v Second Number: %v Total: %v\n", n1, n2, n1*n2)
		return n2, true
	}
	return n2, false
}

func sum3(n1 int, n2 int, n3 int) bool {
	if n1+n2+n3 == 2020 {
		fmt.Printf("First Number: %v Second Number: %v Third Number: %v Total: %v\n", n1, n2, n3, n1*n2*n3)
		return true
	}
	return false
}

func main() {
	var n []int
	data, err := os.Open("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		if err = data.Close(); err != nil {
		}
	}()

	s := bufio.NewScanner(data)
	for s.Scan() {
		idata, err := strconv.Atoi(s.Text())
		if err != nil {
			log.Fatal(err)
		}
		n = append(n, idata)
	}
	oldwin := 0
	for fn := 0; fn < len(n)-1; fn++ {
		for sn := 0; sn < len(n)-1; sn++ {
			if n[sn] != n[fn] && oldwin != n[fn] {
				wnum, winner := sum2(n[fn], n[sn])
				if winner == true {
					oldwin = wnum
				}

			}
		}
	}
	winners := []int{0, 0, 0}
	for fn := 0; fn < len(n)-1; fn++ {
		for sn := 0; sn < len(n)-1; sn++ {
			for tn := 0; tn < len(n)-1; tn++ {
				if sort.IntSlice(winners).Search(n[fn]) == 3 {
					if sum3(n[fn], n[sn], n[tn]) {
						winners[0] = n[fn]
						winners[1] = n[sn]
						winners[2] = n[tn]
					}
				}
			}
		}
	}
}
